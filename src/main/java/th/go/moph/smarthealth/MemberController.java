package th.go.moph.smarthealth;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {

	@Autowired
	MembersRepository memberRepository;
	
	
	@GetMapping("/v1/members")
	public List<Map<String,Object>> getAllMember() {		
		return memberRepository.getAllMember();
	}
	
	@GetMapping("/v1/members/{id}")
	public Map<String,Object> getMemberByID(@PathVariable int id) {		
		return memberRepository.getMemberByID(id);
	}
	
	@PostMapping("/v1/members")
	public int saveMember(@RequestBody Member member) {		
		return memberRepository.saveMember(member);
	}
	
	@PutMapping("/v1/members/{id}")
	public int updateMember(@RequestBody Member member,@PathVariable int id) {		
		return memberRepository.updateMember(member,id);
	}
	
	@DeleteMapping("/v1/members/{id}")
	public int deleteMember(@PathVariable int id) {		
		return memberRepository.deleteMember(id);
	}
}
