package th.go.moph.smarthealth;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/v1/myname")
	public String getName() {
		return "myname";
	}
	
	@GetMapping("/v1/myname/{name}")
	public String getNameByName(@PathVariable String name) {
		return "myname="+name;
	}
	
	@GetMapping("/v2/myname")
	public String getName(String name) {
		return "myname="+name;
	}
	@GetMapping("/v3/myname/{name}/{lastname}")
	public String getNameAndLastname(@PathVariable String name,@PathVariable String lastname) {
		return "name = "+name+ " lastname = "+lastname;
	}
	
	@PostMapping("/v1/myname")
	public String postMyName() {
		return "myname";
	}
	
	@GetMapping("/v1/getAllMembers")
	public List<Map<String,Object>> getAllMember() {
		MembersRepository m = new MembersRepository();
		return m.getAllMember();
	}
	
}
