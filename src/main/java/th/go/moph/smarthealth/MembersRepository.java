package th.go.moph.smarthealth;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MembersRepository{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Map<String,Object>> getAllMember(){
		return jdbcTemplate.queryForList("select * from members");
	}
	
	public Map<String,Object> getMemberByID(int id){
		return jdbcTemplate.queryForMap("select * from members where id = ? ",new Object[] {id});
	}
	
	public int saveMember(Member member){
		String sql = "insert into members(name,lastname,email) values(?,?,?)";
		return jdbcTemplate.update(sql,new Object[] {member.getName(),member.getLastname(),member.getEmail()});
	}
	
	public int updateMember(Member member,int id){
		String sql = "update members set `name` = ? ,`lastname` = ? ,`email` = ? where id = ? ";
		return jdbcTemplate.update(sql,new Object[] {member.getName(),member.getLastname(),member.getEmail(),id});
	}
	
	public int deleteMember(int id){
		String sql = "delete from members where id = ? ";
		return jdbcTemplate.update(sql,new Object[] {id});
	}
}
